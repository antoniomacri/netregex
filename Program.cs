using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Text.RegularExpressions;

using Mono.Options;

namespace netregex
{
    static class main
    {
        private static string app = Path.GetFileName(Assembly.GetEntryAssembly().Location);

        public static int Main(string[] args)
        {
            RegexOptions options = RegexOptions.Compiled;
            bool show_help = false;

            var option_set = new OptionSet() {
                {
                    "i|case-insensitive", "Use case-insensitive matching.",
                    v => options |= RegexOptions.IgnoreCase
                },
                {
                    "m|multiline", "Use multiline mode, where ^ and $ match the beginning and end of each line "
                        + "(instead of the beginning and end of the input string).",
                    v => options |= RegexOptions.Multiline
                },
                {
                    "s|singleline", "Use single-line mode, where the period (.) matches every character "
                        + "(instead of every character except \\n).",
                    v => options |= RegexOptions.Singleline
                },
                {
                    "e|explicit-capture", "Do not capture unnamed groups. The only valid captures are explicitly "
                        + "named or numbered groups of the form (?<name> subexpression).",
                    v => options |= RegexOptions.ExplicitCapture
                },
                {
                    "w|ignore-whitespace", "Eliminate unescaped white space from the pattern (but not in character "
                        + "classes) and enables comments with #.",
                    v => options |= RegexOptions.IgnorePatternWhitespace
                },
                {
                    "c|culture-invariant", "Ignore cultural differences in language.",
                    v => options |= RegexOptions.CultureInvariant
                },
                { "h|help", "Show this message and exit.", v => show_help = v != null },
            };

            List<string> extra;
            try {
                extra = option_set.Parse(args);
            } catch (OptionException e) {
                Console.Write("Error: ");
                Console.WriteLine(e.Message);
                Console.WriteLine("Try `{0} --help' for more information.", app);
                return 1;
            }

            if (show_help || extra.Count != 2) {
                Console.WriteLine("Usage:");
                Console.WriteLine("  {0} [options] <regex> <replace>", app);
                Console.WriteLine();
                Console.WriteLine("Options:");
                option_set.WriteOptionDescriptions(Console.Out);
                return 1;
            }

            string pattern = extra[0];
            string replacement = extra[1];

            Regex regex = new Regex(pattern, options);
            string input;
            while ((input = Console.ReadLine()) != null) {
                Console.WriteLine(regex.Replace(input, replacement));
            }

            return 0;
        }
    }
}
